<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();

	if (is_woocommerce()) {
		while (have_posts()) :
			the_post();

			the_content();
		endwhile;
	} else {
		if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="container" style="padding:80px 0;">
				<div class="row">
					<div class="col-md-12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>

			<?php endwhile;
		endif;
	}

get_footer();
