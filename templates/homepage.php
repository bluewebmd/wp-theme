<?php
/**
 * Template Name: Homepage
 */
get_header();
?>

<div id="slider">
	<ul class="slides">
		<?php
			$slider = new WP_Query('post_type=slider&posts_per_page=5&orderby=menu_order&order=ASC');
			while ($slider->have_posts()) :
				$slider->the_post();
				$slideimg = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "Full");
				$custom = get_post_custom();
				$link = $slide['url'];
				echo '<li class="slide cover" id="slide-' . get_the_ID() . '" style="background-image: url('.$slideimg[0].');"> </li>';
			endwhile;
			wp_reset_postdata();
		?>
	</ul>
</div>

<div class="container-fluid slider-map" style="">
	<div class="container nopadding" id="controls" >
		<ul class="controls nopadding text-center">
			<?php
				$slider = new WP_Query('post_type=slider&posts_per_page=5&orderby=menu_order&order=ASC');
				$count = $slider->post_count;
				$width = (100 / $count) - 1;
				while ($slider->have_posts()) :
					$slider->the_post();
					echo '<li class="text-center" style="width:' . $width . '%;"><a href="javascript:;" data-id="#slide-'.get_the_ID().'">'.get_the_title().'</a></li>';
				endwhile;
				wp_reset_postdata();
			?>
		</ul>
	</div>
</div>

<div class="container">
	<h2 class="nopadding"><?php _e("Servicii", "caricaturi"); ?></h2>
</div>
<div class="container-fluid caricatures-list">
	<div class="container">
		<div class="row">
			<?php
				$terms = get_terms([
					'taxonomy' => 'caricaturi',
    				'hide_empty' => false
				]);
				foreach ( $terms as $term )
				{
					$src = get_field('thumb_caricature', 'caricaturi_' . $term->term_id);
					$product_id = get_field('product', 'caricaturi_' . $term->term_id);
					$url = !empty(get_field('link', 'caricaturi_' . $term->term_id)) ? get_field('link', 'caricaturi_' . $term->term_id) :  get_site_url() . "/caricaturi/" . $term->slug ."/";
					$product = new WC_Product($product_id);
					$price = $product->get_price_html();
					?>
					<div class="col-xs-12 col-sm-6 col-md-4 bordered text-center thumbpadding">
						<a href="<?php echo $url; ?>">
							<div class="thumb" style="background-image:url(<?php echo $src; ?>);">
								<?php if (!empty($price)){ ?>
									<div class="ribbon-wrapper-pink"><div class="ribbon-pink"><span class="price"><?php echo $price; ?></span> </div></div>
								<?php } ?>
							</div>
							<h2 class="text-center activate-step-2 activate-step-h2"><?php echo $term->name;?></h2>
						</a>
					</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php
get_template_part("templates/parts/footer", "widgets");
get_footer();
?>