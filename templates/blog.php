<?php
/**
 * Template Name: Blog
 */
get_header();
?>
<div class="container">

    <div class="row content">
        <div class="col-md-12">
            <?php while (have_posts()) : the_post(); ?>
                <header class="page-title">
                    <h1><?php the_title();?></h1>
                </header>

                <?php the_content(); ?>
                <hr/>

            <?php // reset the loop
            endwhile;
            wp_reset_query(); ?>

            <?php // Blog post query
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            query_posts(array('post_type' => 'post', 'paged' => $paged, 'showposts' => 0));
            if (have_posts()) : while (have_posts()) : the_post(); ?>

                <div <?php post_class(); ?>>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
                        <h3><?php the_title();?></h3>
                    </a>
                    <p class="meta">
                        <?php echo bootstrapwp_posted_on();?>
                    </p>

                    <div class="row">
                        <?php // Post thumbnail conditional display.
                        if ( bootstrapwp_autoset_featured_img() !== false ) :
						$slideimg = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), "Full");
						?>
                            <div class="col-md-4">
                                <a href="<?php the_permalink(); ?>" title="<?php  the_title_attribute( 'echo=0' ); ?>">
                                    <img src="<?php echo $slideimg[0]; ?>" class="img-responsive" alt="<?php echo get_the_title();?>"/>
                                </a>
                            </div>
                        <?php endif; ?>
                            <div class="col-md-8">
                                <?php the_excerpt(); ?>
								<div class="col-md-3" style="padding:10px 0px;"><a class="btn btn-purple btn-lg center-block" href="<?php the_permalink(); ?>"> <?php _e("Mai multe detalii", "caricatura"); ?> </a> </div>
                            </div>
							<div class="col-md-12" style="padding:20px 15px;">

                            </div>

                    </div><!-- /.row -->

                    <hr/>
                </div><!-- /.post_class -->

            <?php // end of blog post loop.
            endwhile; endif; ?>

            <?php bootstrapwp_content_nav('nav-below');?>
        </div>
        </div>
        </div>

<?php get_footer(); ?>