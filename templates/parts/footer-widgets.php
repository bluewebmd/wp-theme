<div class="container visible-xs-block">
	<div class="col-xs-6">
		<div class="text-center">
		    <h3><?php _e( "Valuta", "caricatura" ) ?>:</h3>
		</div>
		<?php echo do_shortcode("[currency_switcher]"); ?>
	</div>
	<div class="col-xs-6">
		<div class="text-center">
		    <h3><?php _e( "Limba", "caricatura" ) ?>:</h3>
		</div>
		<select class="select form-control follow-value">
			<?php
                if (function_exists("icl_get_languages")) {
                    $languages = icl_get_languages('skip_missing=0&orderby=code');
                    if (!empty($languages)) {
                        foreach ($languages as $l) {
                            $selected = ($l['active']) ? ' selected="selected"' : "";
                            echo '<option value="'.$l['url'].'"'.$selected.'>'.$l['native_name'].'</option>';
                        }
                    }
                }
            ?>
		</select>
	</div>
</div>

<div class="container-fluid footer-widgets" style="background-image:url('<?php the_field("footer_background", "option");?>');">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 text-center p-20">
                <h2><?php _e( "Ultimele Caricaturi", "caricatura" ) ?></h2>
                <div class="footer-widgets-row">
                    <?php
                        $caricatures = new WP_Query('post_type=caricatura&posts_per_page=4&orderby=ID&order=DESC');
                        while ($caricatures->have_posts()) :
                            $caricatures->the_post();
                            $src = get_field( "caricatura", $caricatures->ID );
                            echo '<a href="'. get_permalink( get_option( 'woocommerce_shop_page_id' ) ) .'">
                                    <div class="col-xs-12 col-sm-6 text-center plr-0">
                                        <img src="'.$src.'" class="mw-100">
                                    </div>
                                </a>';
                        endwhile;
                        wp_reset_postdata();
                    ?>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 text-center p-20">
                <h2><?php _e( "Oferte", "caricatura" ) ?></h2>
                <?php
                    $args = array(
                        'post_type' => 'banner',
                        'posts_per_page' => '1',
                        'orderby' => 'rand',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'banners',
                                'field'    => 'slug',
                                'terms'    => 'prima-pagina',
                            ),
                        ),
                    );
                    $banner = new WP_Query($args);
                    while ($banner->have_posts()) :
                        $banner->the_post();
                        $src = get_field( "image" );
                        $link = get_field( "link" );
                        ?>
                        <?php if (!empty($link)) { ?>
                            <a href="<?php echo $link; ?>" title="<?php echo get_the_title(); ?>" target="_blank">
                        <?php } ?>
                            <img src="<?php echo $src; ?>" alt="<?php echo get_the_title(); ?>" width="281">
                        <?php if (!empty($link)) { ?>
                            </a>
                        <?php } ?>
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 text-center p-20">
                <h2><?php _e("Facebook", "caricatura"); ?></h2>
                <?php the_field("facebook_box", "option"); ?>
            </div>

            </div>
        </div>
	</div>