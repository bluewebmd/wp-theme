<?php
	get_header();
	the_post();

	$original = get_field( "poza_originala" );
	$caricatura = get_field( "caricatura" );
	$descr = get_field( "descriere_caricatura" );
	$title = get_the_title();

?>

	<div class="container container__padding" itemscope itemtype="http://schema.org/Article">
		<div class="row">
			<div class="col-md-12">
				<h2 itemprop="headline" class="text-center pb-00400"><?php echo $post->post_title; ?></h2>
				<meta itemprop="inLanguage" content="ro" />
				<p itemprop="author" itemscope itemtype="https://schema.org/Person" class="d-none"> De catre <span itemprop="name">Alexandru Placintă</span> </p>
				<meta itemprop="datePublished" content="<?php echo get_the_time( "Y-m-d" ); ?>"/>
				<meta itemprop="dateModified" content="<?php echo get_the_modified_time(  "Y-m-d" ); ?>"/>

				 <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
					<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					  <meta itemprop="url" content="<?php echo get_site_url(); ?>/wp-content/themes/caricatura/images/logo.png">
					  <meta itemprop="width" content="170">
					  <meta itemprop="height" content="170">
					</div>
					<meta itemprop="name" content="<?php echo $post->post_title; ?> Caricatura.MD">
				 </div>
				 <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
					<meta itemprop="url" content="<?php echo $caricatura; ?>">
					<meta itemprop="width" content="600">
					<meta itemprop="height" content="60">
				  </div>
				<div itemprop="articleBody">

					<div class="col-sm-6"> <div class="thumb_caricature" style="background-image:url(<?php echo $original; ?>);"></div> </div>
					<div class="col-sm-6"> <div class="thumb_caricature" style="background-image:url(<?php echo $caricatura; ?>);"></div> </div>
					<div class="col-xs-12 text-center" style="padding:15px 0;"><?php echo $descr; ?></div>

				</div>
				<div class="col-md-12">
					<div class="share-button sharer col-xs-12">
						<div class="social top center networks-5 active text-center">
							<div class="addthis_inline_share_toolbox"></div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 text-center">
					<a href="<?php echo get_permalink(wc_get_page_id('shop')); ?>" class="comanda"><?php _e("Comanda acum!"); ?></a>
				</div>

				<div class="col-xs-12 text-center" style="padding:25px 0;"><div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-numposts="5" width="100%" ></div></div>
			</div>
		</div>
	</div>
	<?php
		get_template_part("footer-widgets");
	?>
<?php get_footer(); ?>