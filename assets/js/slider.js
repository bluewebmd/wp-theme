
var $isMSIE = /*@cc_on!@*/ 0;

function slider($slider) {
    jQuery("li", $slider)
        .first()
        .addClass("active");
    jQuery("#controls li")
        .first()
        .addClass("active");

    jQuery("li .controls a", $slider).click(function () {
        if (jQuery(this).hasClass("prev")) {
            slideBack($slider);
        } else {
            slide($slider);
        }
    });

    jQuery("#controls a").click(function () {
        var id = jQuery(this).data("id");
        jQuery("#controls a")
            .parent()
            .removeClass("active");
        jQuery(this)
            .parent()
            .addClass("active");

        if ($isMSIE) {
            jQuery("ul.slides li.active", $slider)
                .fadeOut()
                .removeClass("active");
            jQuery(id)
                .fadeIn()
                .addClass("active");
        } else {
            jQuery("ul.slides li.active", $slider).removeClass("active");
            jQuery(id).addClass("active");
        }
    });

    interval();

    jQuery($slider).hover(
        function () {
            clearInterval($timer);
        },
        function () {
            $timer = setInterval("slide('#slider')", 7000);
        }
    );
}

function interval() {
    $timer = setInterval("slide('#slider')", 7000);
}

function slide($slider) {
    if (jQuery("ul.slides li.active", $slider).is(":last-child")) {
        if ($isMSIE) {
            jQuery("ul.slides li.active", $slider)
                .fadeOut()
                .removeClass("active");
            jQuery("ul.slides li:first-child", $slider)
                .fadeIn()
                .addClass("active");
        } else {
            jQuery("ul.slides li.active", $slider).removeClass("active");
            jQuery("ul.slides li:first-child", $slider).addClass("active");
        }
    } else {
        if ($isMSIE) {
            jQuery("ul.slides li.active", $slider)
                .fadeOut()
                .removeClass("active")
                .next()
                .fadeIn()
                .addClass("active");
        } else {
            jQuery("ul.slides li.active", $slider)
                .removeClass("active")
                .next()
                .addClass("active");
        }
    }

    jQuery("#controls li").removeClass("active");
    jQuery('a[data-id="#' + jQuery("ul.slides li.active", $slider).attr("id") + '"]')
        .parent()
        .addClass("active");
}

function slideBack($slider) {
    var $isMSIE = /*@cc_on!@*/ 0;

    if (jQuery("ul.slides li.active", $slider).is(":first-child")) {
        if ($isMSIE) {
            jQuery("ul.slides li.active", $slider)
                .fadeOut()
                .removeClass("active");
            jQuery("ul.slides li:last-child", $slider)
                .fadeIn()
                .addClass("active");
        } else {
            jQuery("ul.slides li.active", $slider).removeClass("active");
            jQuery("ul.slides li:last-child", $slider).addClass("active");
        }
    } else {
        if ($isMSIE) {
            jQuery("ul.slides li.active", $slider)
                .fadeOut()
                .removeClass("active")
                .prev()
                .fadeIn()
                .addClass("active");
        } else {
            jQuery("ul.slides li.active", $slider)
                .removeClass("active")
                .prev()
                .addClass("active");
        }
    }

    jQuery("#controls li").removeClass("active");
    jQuery('a[data-id="#' + jQuery("ul.slides li.active", $slider).attr("id") + '"]')
        .parent()
        .addClass("active");
}

function slideTo($slider) {
    var $isMSIE = /*@cc_on!@*/ 0;
}
