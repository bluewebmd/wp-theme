jQuery(document).ready(function($) {

  let $slider = "#slider";
  if ($($slider).length > 0)
    slider($slider);

  $(document).on("click", "#trigger-overlay", function(e) {
    e.preventDefault();
    $("#main-nav").css("width", "100%");
  });

  $(document).on("click", ".closebtn", function(e) {
    e.preventDefault();
    $("#main-nav").css("width", "0%");
  });

  $(".follow-value").on("change", function() {
    var url = $(this).val();
    if (url) window.location = url;
    return false;
  });

  $(window).scroll(function() {
    if ($(this).scrollTop() >= 80) {
      $(".icon-arrow-up").fadeIn(250);
      $("#scroll-menu").fadeIn(250);
    } else {
      $(".icon-arrow-up").fadeOut(250);
      $("#scroll-menu").fadeOut(250);
    }
  });

  $(".icon-arrow-up").click(function(event) {
    event.preventDefault();
    $("html, body").animate(
      {
        scrollTop: 0
      },
      850
    );
  });

  $(document).on("click", ".masonry-item", function(e) {
    var winWidth = window.screen.width,
      url = $(this).data("url");
      console.log(url);
    if (winWidth < 767) {
      window.location.href = url;
    }
  });

  $(document).on("click", ".switch-modal", function(e) {
    var current_id = $(this).data("current_id");
    var modal_id = $(this).data("modal_id");
    console.log(current_id);
    console.log(modal_id);

    jQuery("#caricature_" + current_id).modal("hide");
    jQuery("#caricature_" + modal_id).modal("show");
  });

  $(".modal").on("shown.bs.modal", function (e) {
    // console.log($(this).attr("id"));
    var clean_url = window.location.href.substr(0, window.location.href.indexOf("#"));
    window.history.pushState('page2', 'Title', clean_url);
    var separator = window.location.href.indexOf("?") === -1 ? "?" : "&";
    document.location = window.location.href + "#" + $(this).attr("id");
  });
  $(".modal").on("hidden.bs.modal", function () {
    var clean_url = window.location.href.substr(0, window.location.href.indexOf("#"));
    window.history.pushState('page2', 'Title', clean_url);
    // document.location = window.location.href.substr( 0, window.location.href.indexOf("#")  );
  });

  var regExp = /#caricature_/gm;
  var testString = window.location.href;
  // var variable = getUrlParams(testString);

  if (regExp.test(testString)) {
    var id = testString.match(/#caricature_([A-Za-z0-9-_]+)/);

    // alert(id + "asd " + testString);
    $(id[0]).modal("show");
  }

});