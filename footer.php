    </div>

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<?php dynamic_sidebar( "footer_first" ); ?>
					<div class="col-xs-6 hidden-xs p-5">
                        <div class="text-center">
                            <h4><?php _e("Valuta:", "caricatura"); ?></h4>
                        </div>
                        <?php echo do_shortcode("[currency_switcher]"); ?>
                    </div>
                    <div class="col-xs-6 hidden-xs p-5">
                        <div class="text-center">
                            <h4><?php _e("Limba:", "caricatura"); ?></h4>
                        </div>
		                <select class="select form-control follow-value">
                            <?php
                                if (function_exists("icl_get_languages")) {
                                    $languages = icl_get_languages('skip_missing=0&orderby=code');
                                    if (!empty($languages)) {
                                        foreach ($languages as $l) {
                                            $selected = ($l['active']) ? ' selected="selected"' : "";
                                            echo '<option value="'.$l['url'].'"'.$selected.'>'.$l['native_name'].'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
	                </div>
				</div>
				<div class="col-md-4">
					<?php dynamic_sidebar( "footer_second" ); ?>
				</div>

				<div class="col-md-4">
					<?php dynamic_sidebar( "footer_third" ); ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<p>&copy; <?php echo date('Y'); ?> <a href="http://www.caricatura.md">caricatura.md</a>. <?php _e("Toate drepturile asupra continutului rezervate.", "caricatura"); ?> </p>
					<p>designed and developed by  <a href="https://www.blueweb.md">blueweb.md</a> </p>
				</div>
			</div>
		</div>
	</footer>

	<?php the_field("footer_custom_code", "option"); ?>

	<?php wp_footer(); ?>
	

</body>
</html>
