<?php

register_sidebar(
    [
        'name' => 'Sidebar Widgets',
        'id'   => 'sidebar-widgets',
        'description'   => 'These are widgets for the sidebar.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ]
);

register_sidebar(
    [
        'name' => 'Primul widget jos (Contacte)',
        'id' => 'footer_first',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>'
    ]
);

register_sidebar(
    [
        'name' => 'Al doilea widget jos (Contacte)',
        'id' => 'footer_second',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>'
    ]
);

register_sidebar(
    [
        'name' => 'Al treilea widget jos (Menu)',
        'id' => 'footer_third',
        'before_widget' => '<div >',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>'
    ]
);
