<?php


add_action('init', 'register_types');

function register_types()
{
    register_taxonomy(
        'caricaturi',
        null,
        [
            'labels' => [
                'name' => __('Tipuri Caricaturi'),
                'singular_name' => __('Tip Caricatura'),
            ],
            'rewrite' => true,
            'hierarchical' => true,
            'show_admin_column' => true,
        ]
    );

    register_taxonomy(
        'banners',
        null,
        [
            'labels' => [
                'name' => __('Locatie Bannere'),
                'singular_name' => __('Locatie Baner'),
            ],
            'hierarchical' => true,
            'show_admin_column' => true,
        ]
    );

    register_post_type('slider',
        [
            'label' => 'Slider',
            'public' => true,
            'hierachical' => false,
            'supports' => [
                'title',
                'page-attributes',
                'thumbnail'
            ]
        ]
    );

    register_post_type( 'banner',
        [
            'labels' => [
                'name' => __( 'Bannere' ),
                'singular_name' => __( 'Banner' )
            ],
            'capability_type' => 'post',
            'supports' => [
                'title'
            ],
            'public' => true,
            'has_archive' => false,
            'menu_icon'=> 'dashicons-welcome-widgets-menus',
            'taxonomies' => [
                'banners'
            ]
        ]
    );

    register_post_type('caricatura',
        [
            'labels' => [
                'name' => __('Caricaturi'),
                'singular_name' => __('Caricatura'),
            ],
            'capability_type' => 'post',
            'supports' => ['title'],
            'public' => true,
            'has_archive' => false,
            'menu_icon' => 'dashicons-format-image',
            'taxonomies' => ['caricaturi'],
        ]
    );

}