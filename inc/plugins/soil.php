<?php
$google_analytics_code = get_option("google_analytics_code", "option");
if (empty($google_analytics_code))
    $google_analytics_code = "UA-40951321-3";

add_theme_support('soil-clean-up');
add_theme_support('soil-disable-asset-versioning');
add_theme_support('soil-disable-trackbacks');
add_theme_support('soil-google-analytics', $google_analytics_code);
// add_theme_support('soil-jquery-cdn');
// add_theme_support('soil-js-to-footer');
add_theme_support('soil-nav-walker');
add_theme_support('soil-nice-search');
add_theme_support('soil-relative-urls');