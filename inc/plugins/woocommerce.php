<?php

/**
 * Limit quantity in cart for bookable product
 */
add_filter('woocommerce_quantity_input_args', function ($args, $product) {

    $args['min_value'] = 1;
    $args['max_value'] = 1;

    return $args;
}, 10, 2);

/**
 * Limit quantity in cart for bookable product
 */
add_action('woocommerce_update_cart_validation', function ($passed, $cart_item_key, $values, $quantity) {
    global $woocommerce;

    $product_id = $values['product_id'];
    $woocommerce_max_qty = 1;

    $product_title = get_the_title($product_id);

    if ($quantity > $woocommerce_max_qty) :
        wc_add_notice(sprintf(__('You can add a maximum of %1$s %2$s\'s.', 'woocommerce-max-quantity'), $woocommerce_max_qty, $product_title), 'error');

    $passed = false;
    endif;

    return $passed;
}, 1, 4);

add_filter('woocommerce_form_field_args', function ($args, $key, $value) {
    $args['input_class'] = array('form-control');
    return $args;
}, 10, 3);

add_filter( 'woocommerce_checkout_fields', function ( $fields ) {
    unset($fields['order']['order_comments']);
    return $fields;
});

add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

function change_existing_currency_symbol($currency_symbol, $currency)
{
    switch ($currency) {
        case 'RON':
            $currency_symbol = 'RON';
            break;
    }
    return $currency_symbol;
}
// add_filter('woocommerce_price_trim_zeros', '__return_true');


// add_filter('woocommerce_get_price', 'return_custom_price', $product, 2);

add_filter('wcml_client_currency', function ($currency) {
    $new_currency = get_field("valuta_plata_in_checkout", "option");
    if (is_checkout())
        return $new_currency;
    else
        return $currency;

}, 10, 1);

add_action('woocommerce_before_cart', 'days_of_manufacture');
add_action('woocommerce_before_checkout_form', 'days_of_manufacture', 5);
function days_of_manufacture()
{

    $text = __('<strong>ATENȚIE!</strong> Plata se efectuează în EUR, din cauza metodei de plată selectată.', 'caricatura');

    echo "<div class='woocommerce-info'>$text</div>";
}




// add_filter('woocommerce_new_order',function(){
//     // if(is_checkout()) {


//         global $woocommerce, $sitepress;

//         $currency = str_replace(array(";", "&"), "", get_woocommerce_currency_symbol());

//         setcookie('caricatura_currency', $currency, time() + 24 * 3600, '/');
//         apply_filters(
//             'wcml_switch_currency_exception',
//             false,
//             $currency,
//             'USD'
//         );

//         WC()->session->set('client_currency', 'USD');
//         WC()->session->set('client_currency_' . $sitepress->get_current_language(), 'USD');
//         WC()->session->set('client_currency_switched', true);
//     // }
// });

// add_filter('woocommerce_thankyou', function () {
//     global $sitepress;

//     if (isset($_COOKIE['caricatura_currency'])) {
//         apply_filters(
//             'wcml_switch_currency_exception',
//             false,
//             'USD',
//             $_COOKIE['caricatura_currency']
//         );

//         WC()->session->set('client_currency', $_COOKIE['caricatura_currency']);
//         WC()->session->set('client_currency_' . $sitepress->get_current_language(), $_COOKIE['caricatura_currency']);
//         WC()->session->set('client_currency_switched', true);
//         setcookie("caricatura_currency", "", time() - 3600);
//     }

//     return $order_status;
// }, 10, 3);



