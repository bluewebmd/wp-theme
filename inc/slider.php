<?php

add_action("admin_init", function() {
    add_meta_box("sliderDetails", "Slider Details", "sliderDetails", "slider");
});

function sliderDetails( $post )
{
    $custom = get_post_custom($post->ID);
    $slider = unserialize($custom["slider"][0]);
    ?>
    <style type="text/css">
        #sliderDetails input {
            width: 400px;
        }
    </style>
    <table>
    <tr>
        <td>  <label><strong>Link URL:</strong></label></td>
        <td>  <textarea name="slider[url]" id="slider-url" cols="30" rows="5"><?php echo $slider['url'] ; ?></textarea></td>
    </tr>
    <tr>
        <td>  <label><strong>Link Text:</strong></label></td>
        <td>  <input name="slider[txt]" type="text" value="<?php echo $slider['txt'] ; ?>" /> </td>
    </tr>
    </table>
    <?php
}

add_action('save_post', function() {
    global $post;

    if ( "slider" != get_post_type($post->ID) )
        return;

    update_post_meta($post->ID, "slider", $_POST["slider"]);
});
