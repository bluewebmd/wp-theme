<?php

/**
 * Theme styles & scripts
 */
add_action('wp_enqueue_scripts', function () {
    /**
     * Styles
     */
    // wp_enqueue_style('Raleway', 'https://fonts.googleapis.com/css?family=Raleway:400,700&amp;subset=latin-ext');
    wp_enqueue_style('font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', "3.3.7");
    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', "3.3.7");
    wp_enqueue_style('main', THEME_CSS . 'main.css', ['bootstrap'], THEME_VERSION);
    if( is_user_logged_in() ) {
        wp_enqueue_style('main-admin', THEME_CSS . 'main-admin.css', ['bootstrap'], THEME_VERSION);
    }

    /**
     * Scripts
     */
    wp_enqueue_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', ['jquery'], "3.3.7", true);
    wp_enqueue_script('addthis', 'https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58b5509bf0621914', NULL, "58b5509bf0621914", true);
    wp_enqueue_script('slider', THEME_JS.'slider.js', ['jquery'], THEME_VERSION, true);
    wp_enqueue_script('main', THEME_JS.'main.js', ['jquery', 'slider'], THEME_VERSION, true);

    if (is_product()) {
        wp_enqueue_script('product', THEME_JS . 'product.js', ['jquery'], THEME_VERSION, true);
    }
});