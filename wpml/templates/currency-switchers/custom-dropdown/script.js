jQuery(document).ready(function ($) {
    $('.wcml_currency_switcher select').on('change', function (e) {
        var valueSelected = this.value;
        $.ajax({
            url: wc_add_to_cart_params.ajax_url,
            method: "post",
            data: {
                action: "wcml_switch_currency",
                currency: valueSelected,
                force_switch: 0
            },
            success: function (data) {
                window.location.href = window.location.href;
                console.log(data);
            }
        });
    });
});