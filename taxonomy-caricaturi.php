<?php
	get_header();

	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

	$src = get_field('thumb_caricature', 'caricaturi_' . $term->term_id);
	$product_id = get_field('product', 'caricaturi_' . $term->term_id);
	$product = new WC_Product($product_id);
	$price = $product->get_price_html();

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$caricatures_array = get_posts(
		array(
			'posts_per_page' => 9,
			'post_type' => 'caricatura',
			'orderby' => 'ID',
			'order' => 'DESC',
			'tax_query' => array(
				array(
					'taxonomy' => 'caricaturi',
					'field' => 'term_id',
					'terms' => $term->term_id,
				),
			),
			'paged' => $paged,
		)
	);



	// var_dump($caricatures_array);

	// $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	// $loop = new WP_Query(
	// 	array(
	// 		'post_type' => 'caricatura',
	// 		'orderby' => 'ID',
	// 		'order' => 'DESC',
	// 		'tax_query' => array(
	// 			array(
	// 				'taxonomy' => 'caricaturi',
	// 				'field' => 'term_id',
	// 				'terms' => $term->term_id,
	// 			),
	// 		),
	// 		'paged' => $paged,
	// 	)
	// );
	// if ($loop->have_posts()) :
	// 	$caricatures = [];
	// 	while ($loop->have_posts()) :
	// 		$caricatures[] = [
	// 			'original' => get_field("poza_originala"),
	// 			'caricatura' => get_field("caricatura"),
	// 			'descr' => get_field("descriere_caricatura"),
	// 			'title' => get_the_title(),
	// 			'current_id' => get_the_ID(),
	// 		];
	// 	endwhile;
	// endif;

	// wp_reset_postdata();

	// var_dump($caricatures);
	// die();
?>

<div class="container " style="margin-top:40px;">
	<div class="row" itemscope itemtype="http://schema.org/Product">
		<div class="col-sm-4 sidebar text-center">
				<div class="thumb" style="background-image:url(<?php echo $src; ?>);margin:30px auto;">
					<?php if (!empty($price)){ ?>
						<div class="ribbon-wrapper-pink">
							<div class="ribbon-pink">
								<span class="price" itemprop="price"><?php echo $price; ?></span>
							</div>
						</div>
					<?php } ?>
				</div>
			<div class="share-button sharer col-xs-12" >
				<div class="addthis_inline_share_toolbox"></div>
			</div>

			<div class="col-sm-12 text-center">
				<a href="<?php echo get_permalink($product_id); ?>" class="comanda"><?php _e("Comanda acum!"); ?></a>
			</div>

			<div class="col-sm-12 hidden-xs nopadding text-center">
				<h2><?php _e("Oferte", "caricatura"); ?></h2>
				<?php
					$args = array(
						'post_type' => 'banner',
						'posts_per_page' => '1',
						'orderby' => 'rand',
						'tax_query' => array(
							array(
								'taxonomy' => 'banners',
								'field'    => 'slug',
								'terms'    => 'informatii-caricaturi',
							),
						),
					);
					$banner = new WP_Query($args);
					while ($banner->have_posts()) : $banner->the_post();
						$src = get_field("image" );
						$link = get_field("link" );
						if (!empty($link)) : ?><a href="<?php echo $link; ?>" title="<?php echo get_the_title(); ?>" target="_blank"><?php endif; ?>
							<img itemprop="image" src="<?php echo $src; ?>" alt="<?php echo get_the_title(); ?>">
						<?php if (!empty($link)) : ?></a>
						<?php endif;
				endwhile; ?>
			</div>

			<div class="col-sm-12 hidden-xs nopadding text-center">
				<h2>Facebook</h2>
				<div class="fb-page" data-href="https://www.facebook.com/caricatura.md/?fref=ts" data-tabs="timeline" data-height="370" data-width="281" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/caricatura.md/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/caricatura.md/?fref=ts">Caricaturi</a></blockquote></div>
			</div>
		</div>
		<style>
			.pagination .current {
				background-color:#eee;
			}
		</style>
		<div class="col-sm-8 caricature_descr" style="margin:20px 0;">
			<h2 itemprop="name" class="text-center" style="margin-top:4px;"><?php echo $term->name;?></h2>
			<!--<p itemprop="description"><?php echo $term->description;?></p>-->
			<p itemprop="description"><?php echo get_field('descriere', 'caricaturi_' . $term->term_id);?></p>


			<div class="masonry masonry-columns-3">
			<?php
				if (!empty($caricatures_array))
				{
					$i = 0;
					foreach ($caricatures_array as $caricature)
					{
						$current_id = $caricature->ID;
						$slug = $caricature->post_name;
						$next_slug = ($i < 8) ? $caricatures_array[$i + 1]->post_name : false;
						$prev_slug = ($i > 0) ? $caricatures_array[$i - 1]->post_name : false;
						$title = $caricature->post_title;
						$caricatura = get_field("caricatura", $current_id);
						$original = get_field("poza_originala", $current_id);
						?>
						<div class="masonry-item" data-url="<?php echo get_permalink($current_id); ?>">
							<a href="#" data-toggle="modal" data-target="#caricature_<?php echo $slug; ?>" title="<?php echo $title; ?>">
								<img src="<?php echo $caricatura; ?>" class="img-responsive center-block" alt="<?php echo $title; ?>">
							</a>
							<div id="caricature_<?php echo $slug; ?>"  class="modal" tabindex="<?php echo $i; ?>" role="dialog">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">&times;</button>
											<h2 class="modal-title text-center"><?php echo $title; ?></h2>
										</div>
										<div class="modal-body">
												<div class="col-sm-6">
													<div class="thumb_caricature" style="background-image:url(<?php echo $original; ?>);"></div>
												</div>
												<div class="col-sm-6">
													<div class="thumb_caricature" style="background-image:url(<?php echo $caricatura; ?>);"></div>
												</div>
												<div class="col-xs-12 text-center" style="padding:15px 0;">
													<?php echo $descr; ?>
												</div>

												<ul class="pager" style="padding:0 15px;">
													<li class="previous" >
														<?php if ($prev_slug) { ?>
															<a href="#caricature_<?php echo $slug; ?>" data-current_id="<?php echo $slug; ?>" data-modal_id="<?php echo $prev_slug ; ?>" class="switch-modal" style="border-radius:0;">
																<i class="fa fa-angle-double-left" aria-hidden="true"></i> <?php _e("Precedenta", "caricatura"); ?>
															</a>
														<?php } ?>
													</li>
													<li class="next">
														<?php if ($next_slug) { ?>
															<a href="#caricature_<?php echo $slug; ?>" data-current_id="<?php echo $slug; ?>" data-modal_id="<?php echo $next_slug; ?>" class="switch-modal" style="border-radius:0;">
																<?php _e("Urmatoarea", "caricatura"); ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i>
															</a>
														<?php } ?>
													</li>
												</ul>
												<div class="share-button sharer col-xs-12">
													<div class="social top center networks-5 active text-center">
														<a href="<?php echo get_permalink(wc_get_page_id('shop')); ?>" class="comanda"><?php _e("Comanda acum!", "caricatura"); ?></a>
														<div class="addthis_inline_share_toolbox"></div>
													</div>
												</div>

										</div>
										<div class="modal-footer">
											<div class="fb-comments" data-href="<?php echo get_permalink($current_id); ?>" data-numposts="5" width="100%" ></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						$i++;
					}
				}
			?>
			</div>

			<?php bootstrap_pagination(true); ?>

			<div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-numposts="5" width="100%"></div>
		</div>

	</div>
</div>

<?php get_footer(); ?>