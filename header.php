<?php
$favicon = get_field("favicon", "option");
$logo = get_field("logo", "option");
$scroll_logo = get_field("scroll_logo", "option");
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="shortcut icon" type="image/png" href="<?php echo $favicon; ?>"/>
        <?php wp_head(); ?>
        <?php the_field("header_custom_code", "option"); ?>
    </head>
    <body <?php body_class(); ?>>
        <?php the_field("header_after_body_tag_custom_code", "option"); ?>
        <header id="header" class="nav-down">
            <div class="container-fluid" id="header-bar">
                <div class="container nopadding">
                    <div class="col-xs-2 col-sm-2 col-md-1">
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                            <img src="<?php echo $logo; ?>" alt="<?php echo bloginfo('name'); ?>">
                        </a>
                    </div>
                    <div class="col-xs-8 col-sm-5 col-md-7">
                        <div class="visible-xs text-center pt-7-ml-10">
                            <a href="<?php echo get_permalink( get_option( 'woocommerce_shop_page_id' ) ); ?>" class="btn btn-default btn-lg btn-comanda br-0" ><?php _e("Magazin Online", "caricatura"); ?></a>
                        </div>
                        <nav id="menu">
                            <?php
                                wp_nav_menu(
                                    [
                                        'theme_location' => 'main_nav',
                                        'menu_id' => 'main_nav'
                                    ]
                                );
                            ?>
                        </nav>
                    </div>
                    <div class="col-xs-2 col-visible-xs-2">
                        <div id="trigger-overlay" class="float-right c-p">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </div>
                    </div>

                    <div class="hidden-xs col-sm-5 col-md-4 hidexs col-fff-fs-16">
                            <div class="col-sm-6 p-1-pt-12">
                                <div class="col-sm-6 p-1">
                                    <?php echo do_shortcode("[currency_switcher]"); ?>
                                </div>
                                <div class="col-sm-6 p-1">
                                    <select class="select form-control p-0 follow-value">
                                        <?php
                                            if (function_exists("icl_get_languages")) {
                                                $languages = icl_get_languages('skip_missing=0&orderby=code');
                                                if (!empty($languages)) {
                                                    foreach ($languages as $l) {
                                                        $selected = ($l['active']) ? ' selected="selected"' : "";
                                                        echo '<option value="'.$l['url'].'"'.$selected.'>'.$l['native_name'].'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="hidden-xs col-sm-6 text-right p-0-lh-30">
                                <span class="header-phone">Ro: <?php the_field("numar_telefon_romania", "option");?></span>
                                <span class="header-phone">MD: <?php the_field("numar_telefon_moldova", "option");?></span>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </header>
        <div id="main-nav" class="overlay">
            <a href="javascript:void(0)" class="closebtn">&times;</a>
            <div class="overlay-content">
                <?php
                $menuParameters = array(
                    'menu' => 31,
                    'container'       => false,
                    'echo'            => false,
                    'items_wrap'      => '%3$s',
                    'depth'           => 0,
                );

                echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
                ?>

                <?php echo do_shortcode("[currency_switcher]"); ?>

                <select class="select form-control select_menu follow-value">
                    <?php
                        if (function_exists("icl_get_languages")) {
                            $languages = icl_get_languages('skip_missing=0&orderby=code');
                            if (!empty($languages)) {
                                foreach ($languages as $l) {
                                    $selected = ($l['active']) ? ' selected="selected"' : "";
                                    echo '<option value="'.$l['url'].'"'.$selected.'>'.$l['native_name'].'</option>';
                                }
                            }
                        }
                    ?>
                </select>
        </div>
        </div>

        <div class="clearfix"></div>

        <div class="icon-arrow-up"></div>

        <div id="scroll-menu">
            <div class="container nopadding">
                <div class="row">
                    <div class="col-md-12">

                        <div class="col-xs-2">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" id="logo-scroll">
                                <img src="<?php echo $scroll_logo; ?>" alt="<?php echo bloginfo('name'); ?>">
                            </a>
                        </div>
                        <div class="col-xs-1 hidden-xs text-right"></div>
                        <div class="col-xs-2 hidden-xs text-right">
                            <div class="col-sm-6 p-1-pt-8">
                                    <?php echo do_shortcode("[currency_switcher]"); ?>
                                </div>
                                <div class="col-sm-6 p-1-pt-8">
                                    <select class="select form-control p-0 follow-value">
                                        <?php
                                            if (function_exists("icl_get_languages")) {
                                                $languages = icl_get_languages('skip_missing=0&orderby=code');
                                                if (!empty($languages)) {
                                                    foreach ($languages as $l) {
                                                        $selected = ($l['active']) ? ' selected="selected"' : "";
                                                        echo '<option value="'.$l['url'].'"'.$selected.'>'.$l['native_name'].'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                        </div>
                        <div class="col-xs-7 text-right">
                            <nav class="float-right">
                                <?php
                                    wp_nav_menu(
                                        [
                                            'theme_location' => 'main_nav',
                                            'menu_id' => 'main_nav'
                                        ]
                                    );
                                ?>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="overlay overlay-hugeinc">
            <button type="button" class="overlay-close">Close</button>
            <nav>
                <?php wp_nav_menu('menu=main_nav&container='); ?>
            </nav>
        </div>

        <div class="content-wrapper">