<?php
	get_header();
	the_post();
	$src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_id() ), "full");
// var_dump($GLOBALS['wp_registered_sidebars']);
?>

	<div class="container container__single" itemscope itemtype="http://schema.org/Article">
		<div class="row">
			<div class="col-xs-12 col-sm-9">
				<h2 itemprop="headline"><?php the_title(); ?></h2>
				<meta itemprop="inLanguage" content="ro" />
				<p><?php the_date(); ?> </p> <p itemprop="author" itemscope itemtype="https://schema.org/Person" class="d-none"> De catre <span itemprop="name">Alexandru Placintă</span> </p>
				<meta itemprop="datePublished" content="<?php echo get_the_time( "Y-m-d" ); ?>"/>
				<meta itemprop="dateModified" content="<?php echo get_the_modified_time(  "Y-m-d" ); ?>"/>

				 <div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">
					<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">
					  <meta itemprop="url" content="<?php echo get_field("logo", "option"); ?>">
					  <meta itemprop="width" content="170">
					  <meta itemprop="height" content="170">
					</div>
					<meta itemprop="name" content="Caricatura.MD">
				 </div>
				 <div itemprop="image" itemscope itemtype="https://schema.org/ImageObject">
					<meta itemprop="url" content="<?php echo $src[0];?>">
					<meta itemprop="width" content="600">
					<meta itemprop="height" content="60">
				  </div>
				<div itemprop="articleBody"><?php the_content(); ?></div>

			</div>
			<div class="col-xs-12 col-sm-3">
				<?php
					echo get_sidebar("sidebar-widgets");
				?>
			</div>
			<div class="col-md-12">
				<div class="share-button sharer col-xs-12">
					<div class="social top center networks-5 active text-center">
						<div class="addthis_inline_share_toolbox"></div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-numposts="5" width="100%"></div>
			</div>

		</div>
	</div>
	<?php
		get_template_part("footer-widgets");
	?>

<?php get_footer(); ?>