<?php

add_theme_support('post-thumbnails');
add_theme_support('title-tag');
add_theme_support( 'woocommerce' );

define('THEME_DIR', untrailingslashit(get_template_directory()));
define('THEME_URL', untrailingslashit(get_stylesheet_directory_uri()));
define('THEME_IMG', THEME_URL . '/assets/img/');
define('THEME_CSS', THEME_URL . '/assets/css/');
define('THEME_JS', THEME_URL . '/assets/js/');
define('THEME_VERSION', 1.3);

if (version_compare(PHP_VERSION, "5.4.0", "<"))
{
    add_action('admin_notices', function() {
        echo '<div class="error">';
        echo '<p>Sorry this theme use some <b>PHP VERSION 5.4</b> functionality. If you want to use this plugin please update your server <b>PHP VERSION 5.4</b> or higher.</p>';
        echo '</div>';
    });

    return;
}

include THEME_DIR."/inc/sidebar.php";
include THEME_DIR."/inc/enqueue.php";
include THEME_DIR."/inc/menu.php";
include THEME_DIR."/inc/custom-post-types.php";
include THEME_DIR."/inc/slider.php";
include THEME_DIR."/inc/helpers.php";
include THEME_DIR."/inc/plugins/soil.php";
include THEME_DIR."/inc/plugins/woocommerce.php";

include THEME_DIR."/inc/admin/theme-settings.php";

add_action('template_redirect', function () {
    if (!class_exists('WPSEO_Frontend')) {
        return;
    }

    $instance = WPSEO_Frontend::get_instance();
    if (!method_exists($instance, 'debug_mark')) {
        return;
    }
    remove_action('wpseo_head', array($instance, 'debug_mark'), 2);
});

add_filter('body_class', function ($classes) {
    global $post;

    if (isset($post->post_name) && !empty($post->post_name)) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }

    return $classes;
});

add_filter('upload_size_limit', function($bytes) {
    return 3145728; // 3 megabytes
});
