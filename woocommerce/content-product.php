<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$image = wp_get_attachment_image_src(get_post_thumbnail_id($product->get_ID()), 'full');
$price = $product->get_price_html();
?>
<div class="col-sm-6 col-md-4 center-block text-center shop_caricature caric<?php echo $product->get_ID(); ?>">
	<a href="<?php echo get_permalink(); ?>" class="shop_caricature-link">
		<div class="thumb" style="background-image:url(<?php echo $image[0]; ?>);">
			<?php if (!empty($price)) { ?>
				<div class="ribbon-wrapper-pink">
					<div class="ribbon-pink">
						<span class="price"><?php echo $price; ?></span>
					</div>
				</div>
			<?php } ?>
		</div>
		<h3 class="text-center shop_caricature-title"><?php echo $product->get_title(); ?></h3>
	</a>
</div>