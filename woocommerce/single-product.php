<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<div class="shop_wrapper">
	<div class="container">
		<ul class="nav nav-pills nav-justified thumbnail setup-panel">
				<li>
					<a href="<?php echo get_permalink(wc_get_page_id('shop')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 1</h4>
						<p class="list-group-item-text"><?php _e("Alege tip caricatură", "caricatura"); ?></p>
					</a>
				</li>
				<li class="active">
					<a href="<?php echo get_permalink(); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 2</h4>
						<p class="list-group-item-text"><?php _e("Detalii comanda", "caricatura"); ?></p>
					</a>
				</li>
				<li class="disabled">
					<a href="<?php echo get_permalink(wc_get_page_id('cart')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 3</h4>
						<p class="list-group-item-text"><?php _e("Coș de cumpărături", "caricatura"); ?></p>
					</a>
				</li>
				<li class="disabled">
					<a href="<?php echo get_permalink(wc_get_page_id('checkout')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 4</h4>
						<p class="list-group-item-text"><?php _e("Checkout", "caricatura"); ?></p>
					</a>
				</li>
			</ul>
	</div>
</div>
<div class="row well mb-0">
	<div class="container">
		<div class="col-md-12 nopadding mb-0">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

		</div>
	</div>
</div>


<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
