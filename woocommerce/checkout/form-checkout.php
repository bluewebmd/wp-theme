<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
	return;
}

?>

<div class="shop_wrapper">
	<div class="container">
		<ul class="nav nav-pills nav-justified thumbnail setup-panel">
				<li>
					<a href="<?php echo get_permalink(wc_get_page_id('shop')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura"); ?> 1</h4>
						<p class="list-group-item-text"><?php _e("Alege tip caricatură", "caricatura"); ?></p>
					</a>
				</li>
				<li>
					<a href="javascript:void(0)">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura"); ?> 2</h4>
						<p class="list-group-item-text"><?php _e("Detalii comanda", "caricatura"); ?></p>
					</a>
				</li>
				<li>
					<a href="<?php echo get_permalink(wc_get_page_id('cart')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura"); ?> 3</h4>
						<p class="list-group-item-text"><?php _e("Coș de cumpărături", "caricatura"); ?></p>
					</a>
				</li>
				<li class="active">
					<a href="<?php echo get_permalink(wc_get_page_id('checkout')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura"); ?> 4</h4>
						<p class="list-group-item-text"><?php _e("Checkout", "caricatura"); ?></p>
					</a>
				</li>
			</ul>
	</div>
</div>
<div class="container">
	<div class="col-md-12 nopadding mb-0">
			<?php
		do_action('woocommerce_before_checkout_form', $checkout);
		?>
	</div>

</div>
<div class="row well mb-0">
	<div class="container">
		<div class="col-md-12 text-nopadding mb-0">
			<h1 class="text-center"><?php _e("Checkout", "caricatura"); ?></h1>
				<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

					<?php if ( $checkout->get_checkout_fields() ) : ?>

						<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

						<div class="col2-set" id="customer_details">
							<div class="col-1">
								<?php do_action( 'woocommerce_checkout_billing' ); ?>

								<?php do_action('woocommerce_checkout_shipping'); ?>
							</div>

							<div class="col-2">


								<h3 id="order_review_heading"><?php esc_html_e('Your order', 'woocommerce'); ?></h3>

								<?php do_action('woocommerce_checkout_before_order_review'); ?>

								<div id="order_review" class="woocommerce-checkout-review-order">
									<?php do_action('woocommerce_checkout_order_review'); ?>
								</div>

								<?php do_action('woocommerce_checkout_after_order_review'); ?>
							</div>
						</div>

						<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

					<?php endif; ?>



				</form>
		</div>
	</div>
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
