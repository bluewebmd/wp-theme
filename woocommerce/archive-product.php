<?php

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

?>
<div class="shop_wrapper">
	<div class="container">
		<ul class="nav nav-pills nav-justified thumbnail setup-panel">
				<li class="active">
					<a href="<?php echo get_permalink(wc_get_page_id('shop')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 1</h4>
						<p class="list-group-item-text"><?php _e("Alege tip caricatură", "caricatura"); ?></p>
					</a>
				</li>
				<li class="disabled">
					<a href="#">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 2</h4>
						<p class="list-group-item-text"><?php _e("Detalii comanda", "caricatura"); ?></p>
					</a>
				</li>
				<li class="disabled">
					<a href="<?php echo get_permalink(wc_get_page_id('cart')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 3</h4>
						<p class="list-group-item-text"><?php _e("Coș de cumpărături", "caricatura"); ?></p>
					</a>
				</li>
				<li class="disabled">
					<a href="<?php echo get_permalink(wc_get_page_id('checkout')); ?>">
						<h4 class="list-group-item-heading"><?php _e("PASUL", "caricatura");?> 4</h4>
						<p class="list-group-item-text"><?php _e("Checkout", "caricatura"); ?></p>
					</a>
				</li>
			</ul>
	</div>
</div>
<div class="row well mb-0">
	<div class="container">
		<div class="col-md-12 text-center nopadding mb-0">
			<?php
				if ( woocommerce_product_loop() ) {

					if ( wc_get_loop_prop( 'total' ) ) {
						?>
						<h1 class="text-center"><?php _e("Alege tipul caricaturii", "caricatura"); ?></h1>
						<?php
						while ( have_posts() ) {
							the_post();

							do_action( 'woocommerce_shop_loop' );

							wc_get_template_part( 'content', 'product' );
						}
					}

				} else {
					do_action( 'woocommerce_no_products_found' );
				}
				do_action( 'woocommerce_after_main_content' );
			?>
		</div>
	</div>
</div>

<?php

get_footer( 'shop' );
